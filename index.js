#!/usr/bin/env node
const argv = require('yargs').argv
const Spider = require('node-spider')
const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

const adapter = new FileSync('db.json')
const db = low(adapter)
const parse = require('parse-domain')
// const parse = require('url').parse


let spider = new Spider({
	// How many requests can be run in parallel
	concurrent: 5,
	// How long to wait after each request
	delay: 0,
	// A stream to where internal logs are sent, optional
	logs: process.stderr,
	// Re-visit visited URLs, false by default
	allowDuplicates: false,
	// If `true` all queued handlers will be try-catch'd, errors go to `error` callback
	catchErrors: true,
	// If `true` the spider will set the Referer header automatically on subsequent requests
	addReferrer: false,
	// If `true` adds the X-Requested-With:XMLHttpRequest header
	xhr: false,
	// If `true` adds the Connection:keep-alive header and forever option on request module
	keepAlive: false,
	// Called when there's an error, throw will be used if none is provided
	error: function(err, url) {
	    console.log(err)
	},
	// Called when there are no more requests
	done: function() {
	},

	//- All options are passed to `request` module, for example:
	headers: { 'user-agent': 'Mozilla/5.0 (X11; CrOS x86_64 10066.0.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36' },
	encoding: 'utf8'
});

const handleRequest = function(doc) {
	// new page crawled
    const domainName = parse(doc.url).domain + '.' + parse(doc.url).tld

    db.defaults({
        url: [{
        	name: '',
        	title: ''
        }]
    })
    .write()
    
    let findUrl = db.get('url')
                    .find({ name : domainName })
                    .value()
                    
    
    
    if (!findUrl) {

        db.get('url')
        .push({
        	name: domainName,
        	title: doc.$('title').text()
        })
        .write()
    } else {
        console.log('skiping save: domain already in db %s', domainName)
    }
    
  
  
    
	console.log(doc.url); // page url
	// uses cheerio, check its docs for more info
	doc.$('a').each(function(i, elem) {
		// do stuff with element
		var href = doc.$(elem).attr('href').split('#')[0];
		var url = doc.resolve(href);
		// crawl more
		if (
			parse(url) &&
			parse(url).domain !== 'facebook' &&
			parse(url).domain !== 'twitter' &&
			parse(url).domain !== 'instagram'
			) {
				try {
					
					spider.queue(url, handleRequest);
				} catch (e) {
					console.log('App crashed while queueing new link')
					console.log(e)
				}
			}
	});
};

// start crawling
if (!argv.domain || !argv.domain.length) {
	throw new Error('Specify a starting domain argument using --domain eg ./index.js --domain="abc.com"')
}
const startingUrl = argv.domain
try {
	
	spider.queue(startingUrl, handleRequest);
} catch (e) {
	console.log(`Crash type Error has occured at `)
	console.log(e)
}